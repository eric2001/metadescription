# MetaDescription

## Description
MetaDescription is a plugin for [Gallery 3](http://gallery.menalto.com/) which will add a few HTML meta tags to the top of your album and photo pages.  This will create a meta DESCRIPTION tag and set it to the description field of the current item, and a KEYWORDS tag which will be set to any tags that the current item has.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
This installs like any other Gallery 3 module.  Download and extract it into your modules folder.  Afterwards log into the Gallery web interface and enable in under the Admin -> Modules menu.

## History
**Version 1.4.0:**
> - Update code responsible for warning the admin that the Tags module is required to current Gallery standards.
> - Fixed a bug that prevented meta tags from being displayed on tag pages.
> - Fully tested against Gallery 3.0.2.
> - Released 07 June 2011.
>
> Download: [Version 1.4.0](/uploads/166663be2330c97ffe0f1e2ad9e0119e/metadescription140.zip)

**Version 1.3.2:**
> - Minor changes for W3C compliance.
> - Released 29 October 2010.
>
> Download: [Version 1.3.2](/uploads/543bced29e6b6b696909d8f8c405967c/metadescription132.zip)

**Version 1.3.1:**
> - Made the HTML tags lower-case so the module would be XHTML 1.0 Transitional compliant.
> - Released 14 June 2010.
>
> Download: [Version 1.3.1](/uploads/e42334a0ea73e3f3666ef691599d70b6/metadescription131.zip)

**Version 1.3.0:**
> - Updated for recent API changes in Gallery 3.
> - Now displays a message alerting the gallery admin that the Tags module is required in order for metadescription to work properly.
> - Released 19 January 2010.
>
> Download: [Version 1.3.0](/uploads/da9f56278204bcafa63944eebec04f10/metadescription130.zip)

**Version 1.2.1:**
> - Replace p::clean with html::clean for recent Gallery API change.
> - Released 31 August 2009
>
> Download: [Version 1.2.1](/uploads/91251505c912838dcbf2dbfa3da57e8a/metadescription121.zip)

**Version 1.2.0:**
> - Updated to support Tag pages.
> - Released 12 August 2009
>
> Download: [Version 1.2.0](/uploads/99fe2e314b5adf0d40efe6e2cbcd4ba0/metadescription120.zip)

**Version 1.1.1:**
> - Added code to use the image/album title for the KEYWORDS meta tag when there are no item tags to use.
> - Released 02 August 2009
>
> Download: [Version 1.1.1](/uploads/9e4119f3a21642c922742e19c9a090f0/metadescription111.zip)

**Version 1.1.0:**
> - Added code to strip HTML from the description.
> - Added code to strip Line Breaks from the description and replace them with a space.
> - Added code to limit the description to 150 characters.
> - Added code to use the image/album title when description is empty.
> - Released 31 July 2009
>
> Download: [Version 1.1.0](/uploads/60721e13d5b09fdec836bc78ffcdc202/metadescription110.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 26 July 2009
>
> Download: [Version 1.0.0](/uploads/fcf93683e569da357ad3322e232c4f8a/metadescription100.zip)
